﻿# About GASDK Demo

This is a google assistant sdk demo app in Python.

It is directly based on, and copied from, a google assisant repo, [googlesamples/assistant-sdk-python](https://github.com/googlesamples/assistant-sdk-python).



* [Google Assistant SSDK Overview](https://developers.google.com/assistant/sdk/overview)
* [Download the Library and Run the Sample](https://developers.google.com/assistant/sdk/develop/python/run-sample)





